var http = require("http");
var fs = require('fs');

var text = fs.readFileSync('index.html', 'utf8');
var style = fs.readFileSync('stylesheet.css', 'utf8');

http.createServer(function(request, response) {
  //logging request
  console.log('Request for '+request.url+' resolved.');
  
  //forming response
  if (request.url=='/') {
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write(text);
} else if (request.url=='/stylesheet.css') {
  response.writeHead(200, {"Content-Type": "text/css"});
  response.write(style);
} else {
  response.writeHead(404, {"Content-Type": "text/plain"});
  response.write("404: url not found!");
}
  response.end();
}).listen(process.env.PORT, process.env.IP);